window.onload = function() {
    console.log('bildwarner start');

    if (window.location.indexOf('bild.de') !== -1) {
        //You are already lost...
        return;
    }

    var body = document.body;
    if (!body) {
        //strange html page...
        return;
    }


    var bild_regexes = [
        RegExp(/ie\ (die\ )?(\"|„|‚)?Bild(-Zeitung)?(\"|“|‘)?\ ((berichtet)|(schreibt))/),
        RegExp(/ie\ Bild\ am\ Sonntag\ ((berichtet)|(schreibt))/)
        RegExp(/((Angaben)|(Informationen))\ ((der)|(von))\ Bild/),
        RegExp(/laut\ ((Bild)|(BamS))/),
        RegExp(/laut\ (B|b)(I|i)(L|l)(D|d)\.de/),
        RegExp(/((Bild)|(BamS))\ schreibt/),
    ];


    //Get the body text and look for matching phrases:
    var text = body.text();
    console.log(text);

    for (regex of bild_regexes) {
        if (regex.test(text)) {
            //We have a match.
            browser.notifications.create(
                'bildwarner',
                {
                    'type': 'basic',
                    'title': 'Bild-Warnung!',
                    'message': 'Diese Seite bezieht sich auf Informationen der Bild-„Zeitung“ oder von bild.de!'
                }
            );
            return;
        }
    }
};

